# CatLoadingView

### 项目介绍

一个小猫形状的自定义动画加载控件


### 效果图

![](image/cat-loading.gif)


### 安装教程

#### 方式一

 1. 下载[catloadinglibrary](https://gitee.com/archermind-ti/cat-loading-view/releases)的har包

 2. 将CatLoadingView包放入需要的module的libs下

 3. 在module中添加对这个har包的引用

    ```groovy
    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    	……
    }
    ```

 4. gradle sync

#### 方式二

3. 在module的build.gradle中添加对`CatLoadingView`的依赖

   ```groovy
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    
       ……
       implementation 'com.gitee.archermind-ti:catloadinglibrary:1.0.1'
   }
   ```

4. 在project的build.gradle中添加`mavenCentral()`的引用

   ``` groovy
   allprojects {
       repositories {
           ……
           mavenCentral()
       }
   }
   ```



### 使用说明

1. 实例化CatLoadingView
```java
        CatLoadingView catLoadingView = new CatLoadingView(this);
```

2.显示
```java
        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .add(ResourceTable.Id_mainstack, catLoadingView)
                .submit();
```

#### Sample

示例代码在`entry` module 中。


### 版本迭代

* v1.0.1

## License

	The MIT License (MIT)

	Copyright © 2015 Roger

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
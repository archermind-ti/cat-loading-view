package com.Rogero0o.catloadingview.slice;

import com.Rogero0o.catloadingview.ResourceTable;
import com.romannurik.catloadinglibrary.CatLoadingView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        //设置状态栏颜色
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.rgb(48,63,159));

        findComponentById(ResourceTable.Id_btn_Show).setClickedListener(component -> {
            CatLoadingView catLoadingView = new CatLoadingView(this);
            catLoadingView.onCreateDialog();
            addFraction(catLoadingView);
        });
    }

    private void addFraction(CatLoadingView catLoadingView) {
        ((FractionAbility) getAbility()).getFractionManager()
                .startFractionScheduler()
                .add(ResourceTable.Id_mainstack, catLoadingView)
                .submit();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        System.out.println("------------[onInactive Ability]-------");
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}

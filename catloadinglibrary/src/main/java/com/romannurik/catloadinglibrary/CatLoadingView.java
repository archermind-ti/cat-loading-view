package com.romannurik.catloadinglibrary;


import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

/**
 * Created by Administrator on 2016/3/30.
 */
public class CatLoadingView extends Fraction {

    private Context context;
    private AnimatorValue operatingAnim, eye_left_Anim, eye_right_Anim;
    private CommonDialog mDialog;
    private Component mouse, eye_left, eye_right;
    private EyelidView eyelid_left, eyelid_right;
    private GraduallyTextView mGraduallyTextView;
    private float progress;
    String text = "";
    boolean isLoading;
    Color color = new Color(Color.rgb(208, 206, 209));

    public CatLoadingView(Context context) {
        this.context = context;
    }

    public void onCreateDialog() {
        if (mDialog == null) {
            Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_catloading_main, null, false);
            mDialog = new CommonDialog(context);
            mDialog.setContentCustomComponent(component);
            mDialog.setSize(200 * 3, 240 * 3);
            mDialog.setAutoClosable(true);
            mDialog.setTransparent(true);

            mDialog.setDestroyedListener(() -> {
                isLoading = false;
                eyelid_left.stopLoading();
                eyelid_right.stopLoading();
                mGraduallyTextView.stopLoading();
                mDialog = null;
                System.gc();
            });

            Component view = mDialog.getContentCustomComponent();
            mGraduallyTextView = (GraduallyTextView) view.findComponentById(ResourceTable.Id_graduallyTextView);
            mouse = view.findComponentById(ResourceTable.Id_mouse);

            eye_left = view.findComponentById(ResourceTable.Id_eye_left);
            eyelid_left = (EyelidView) view.findComponentById(ResourceTable.Id_eyelid_left);
            eyelid_left.setColor(color);

            eye_right = view.findComponentById(ResourceTable.Id_eye_right);
            eyelid_right = (EyelidView) view.findComponentById(ResourceTable.Id_eyelid_right);
            eyelid_right.setColor(color);

            operatingAnim = new AnimatorValue();
            operatingAnim.setLoopedCount(Animator.INFINITE);
            operatingAnim.setDuration(2000);
            operatingAnim.setValueUpdateListener((animatorValue, v) -> {
                this.progress = v * -360;
                mouse.setRotation(progress);
            });

            eye_left_Anim = new AnimatorValue();
            eye_left_Anim.setLoopedCount(Animator.INFINITE);
            eye_left_Anim.setDuration(2000);
            eye_left_Anim.setValueUpdateListener((animatorValue, v) -> {
                this.progress = v * -360;
                eye_left.setRotation(progress);
                eyelid_left.setProgress(v);
            });

            eye_right_Anim = new AnimatorValue();
            eye_right_Anim.setLoopedCount(Animator.INFINITE);
            eye_right_Anim.setDuration(2000);
            eye_right_Anim.setValueUpdateListener((animatorValue, v) -> {
                this.progress = v * -360;
                eye_right.setRotation(progress);
                eyelid_right.setProgress(v);
            });

            operatingAnim.setCurveType(Animator.CurveType.LINEAR);
            eye_left_Anim.setCurveType(Animator.CurveType.LINEAR);
            eye_right_Anim.setCurveType(Animator.CurveType.LINEAR);

            operatingAnim.setStateChangedListener(new AnimatorListenerAdapter() {
                @Override
                public void onRepeat(Animator animator) {
                    super.onRepeat(animator);
                    eyelid_left.resetAnimator();
                    eyelid_right.resetAnimator();
                }
            });

            if (!text.isEmpty()) {
                mGraduallyTextView.setText(text);
            }
            mDialog.show();
        }
    }

    @Override
    protected void onActive() {
        super.onActive();
        initListeners(mouse);
        initListeners(eye_left);
        initListeners(eye_right);
        isLoading = true;
        eyelid_left.startLoading();
        eyelid_right.startLoading();
        mGraduallyTextView.startLoading();
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        System.out.println("------------[onInactive Fraction]-------");
    }

    public void setText(String str) {
        text = str;
    }

    private void initListeners(Component component) {
        component.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                startAnimation();
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {
                stopAnimation();
            }
        });
    }

    private void startAnimation() {
        operatingAnim.start();
        eye_left_Anim.start();
        eye_right_Anim.start();
    }

    private void stopAnimation() {
        operatingAnim.stop();
        eye_left_Anim.stop();
        eye_right_Anim.stop();
    }
}

package com.romannurik.catloadinglibrary;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Created by Administrator on 2016/3/30.
 */
public class EyelidView extends Image implements Component.DrawTask {

    private float progress;
    private boolean isLoading;
    private Paint mPaint;
    private boolean isStop = true;
    private int duration = 1000;
    private AnimatorValue valueAnimator;

    public EyelidView(Context context) {
        super(context);
        init();
    }

    public EyelidView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public EyelidView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        mPaint = new Paint();
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        addDrawTask(this);
        setEnabled(false);
        setTouchFocusable(false);

        valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(duration);
        valueAnimator.setLoopedCount(0);
    }

    public void setColor(Color color) {
        mPaint.setColor(color);
    }

    public void startLoading() {
        if (!isStop) {
            return;
        }
        isLoading = true;
        isStop = false;
        valueAnimator.start();
    }

    public void resetAnimator() {
        valueAnimator.start();
    }

    public void stopLoading() {
        isLoading = false;
        valueAnimator.end();
        valueAnimator.cancel();
        isStop = true;
    }

    private final float PROGRESS_SPACE = 0.1f;

    public void setProgress(float animProgress) {
        if (animProgress > PROGRESS_SPACE && animProgress <= 0.5f) {
            progress = (animProgress - PROGRESS_SPACE) / (0.5f - PROGRESS_SPACE);
        } else if (animProgress > 0.5f && animProgress < 1.0f - PROGRESS_SPACE) {
            progress = (0.5f - PROGRESS_SPACE - (animProgress - 0.5f)) / (0.5f - PROGRESS_SPACE);
        }
        getContext().getUITaskDispatcher().asyncDispatch(this::invalidate);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (!isStop) {
            float bottom;
            bottom = (1.0f - progress) * getHeight();
            bottom = Math.min(bottom, (getHeight() / 2f));
            canvas.drawRect(0, 0, getWidth(), bottom, mPaint);
        }
    }

    @Override
    public void setLayoutRefreshedListener(LayoutRefreshedListener listener) {
        super.setLayoutRefreshedListener(listener);
        if (!isLoading) {
            return;
        }
        if (getVisibility() == Component.VISIBLE) {
            valueAnimator.resume();
        } else {
            valueAnimator.pause();
        }
    }
}

package com.romannurik.catloadinglibrary;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Created by Administrator on 2016/3/30.
 */
public class GraduallyTextView extends Text implements Component.DrawTask {

    private String text;
    private String finalText;
    private int startY = 0;
    private float progress;
    private boolean isLoading;
    private Paint mPaint;
    private int textLength;
    private boolean isStop = true;
    private float scaleX;
    private int duration = 2000;
    private float sigleDuration;

    private AnimatorValue valueAnimator;


    public GraduallyTextView(Context context) {
        super(context);
        init();
    }


    public GraduallyTextView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }


    public GraduallyTextView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        mPaint = new Paint();
        mPaint.setColor(Color.WHITE);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        addDrawTask(this);
        setEnabled(false);
        setTouchFocusable(false);
        valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(duration);
        valueAnimator.setDelay(1000);
        valueAnimator.setLoopedCount(Animator.INFINITE);
        valueAnimator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        float endSpeed = 100;
        float startSpeed = 0;
        float factor = endSpeed - startSpeed;
        valueAnimator.setValueUpdateListener((animatorValue, v) -> {
            if (v >= 0.97f) {
                text = finalText;
            } else {
                text = finalText.substring(0, (int) (finalText.length() * v));
            }
            this.progress = factor * v + startSpeed;
            getContext().getUITaskDispatcher().asyncDispatch(() -> invalidate());
        });
    }

    public void startLoading() {
        if (!isStop) {
            return;
        }
        textLength = getText().length();
        if (getText().isEmpty()) {
            return;
        }
        isLoading = true;
        isStop = false;
        finalText = text = getText();
        scaleX = getScaleX() * 10;
        startY = 38;
        mPaint.setTextSize(getTextSize());
        setText("");
        setHint("");
        valueAnimator.start();
        sigleDuration = 100f / textLength;
    }


    public void stopLoading() {
        isLoading = false;
        valueAnimator.end();
        valueAnimator.cancel();
        isStop = true;
        setText(finalText);
    }

    @Override
    public void setLayoutRefreshedListener(LayoutRefreshedListener listener) {
        super.setLayoutRefreshedListener(listener);
        if (!isLoading) {
            return;
        }
        if (getVisibility() == Component.VISIBLE) {
            valueAnimator.resume();
        } else {
            valueAnimator.pause();
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (!isStop) {
            mPaint.setAlpha(255);
            if (progress / sigleDuration >= 1) {
                canvas.drawText(mPaint, String.valueOf(text), scaleX, startY);
            }
            mPaint.setAlpha((int) (255 * ((progress % sigleDuration) / sigleDuration)));
            int lastOne = (int) (progress / sigleDuration);
            if (lastOne < textLength) {
                canvas.drawText(mPaint, String.valueOf(text), scaleX, startY);
            }
        }
    }
}
